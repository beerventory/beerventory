<!DOCTYPE html>
<html>

<head>
    <title>Bierlager Management</title>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="#">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/jquery-ui.min.css">
    <link rel="stylesheet" href="css/fonts.css">
    <link rel="stylesheet" href="css/styles.css">

    <script src="js/jquery-3.4.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery-ui.min.js"></script>
    <script src="js/handlebars-4.5.3.min.js"></script>
    <script src="js/scripts.js"></script>
</head>

<body>
    <nav class="nav-top">
        <ul>
            <li><a href="#stock_insert">Bier hinzufügen</a></li>
            <li><a href="#stock">Bierlager</a></li>
            <li><a href="#stats">Statistiken</a></li>
        </ul>
    </nav>

    <div id="output" class="container"></div>

<script id="template_stock_insert" type="text/x-handlebars-template">
    <form>
        <div class="col-sm-8">
            <fieldset class="form-group">
                <legend class="col-form-label col-sm-12 pt-0">Bier Informationen</legend>
                <div class="form-group row">
                    <label for="name" class="col-sm-2 col-form-label">Name</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control ui-widget-content ui-corner-all" id="name" name="name">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="brewery" class="col-sm-2 col-form-label">Brauerei</label>
                    <div class="col-sm-10">
                        <input type="text" id="brewery" name="brewery" class="form-control ui-widget-content ui-corner-all">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="country" class="col-sm-2 col-form-label">Land</label>
                    <div class="col-sm-10">
                        <select name="country" id="country" class="form-control ui-widget-content ui-corner-all">
                            <option value="CH" selected>Schweiz</option>
                            <option value="BE">Belgien</option>
                            <option value="DK">Dänemark</option>
                            <option value="GB">Grossbritannien</option>
                            <option value="CA">Kanada</option>
                            <option value="NO">Norwegen</option>
                            <option value="US">Vereinigte Staaten von Amerika (USA)</option>
                            <optgroup label="A">
                                <option value="AF">Afghanistan</option>
                                <option value="EG">Ägypten</option>
                                <option value="AX">Åland</option>
                                <option value="AL">Albanien</option>
                                <option value="DZ">Algerien</option>
                                <option value="AS">Amerikanisch-Samoa</option>
                                <option value="VI">Amerikanische Jungferninseln</option>
                                <option value="AD">Andorra</option>
                                <option value="AO">Angola</option>
                                <option value="AI">Anguilla</option>
                                <option value="AQ">Antarktis</option>
                                <option value="AG">Antigua und Barbuda</option>
                                <option value="GQ">Äquatorialguinea</option>
                                <option value="AR">Argentinien</option>
                                <option value="AM">Armenien</option>
                                <option value="AW">Aruba</option>
                                <option value="AC">Ascension</option>
                                <option value="AZ">Aserbaidschan</option>
                                <option value="ET">Äthiopien</option>
                                <option value="AU">Australien</option>
                            </optgroup>
                            <optgroup label="B">
                                <option value="BS">Bahamas</option>
                                <option value="BH">Bahrain</option>
                                <option value="BD">Bangladesch</option>
                                <option value="BB">Barbados</option>
                                <option value="BY">Belarus (Weissrussland)</option>
                                <option value="BE">Belgien</option>
                                <option value="BZ">Belize</option>
                                <option value="BJ">Benin</option>
                                <option value="BM">Bermuda</option>
                                <option value="BT">Bhutan</option>
                                <option value="BO">Bolivien</option>
                                <option value="BA">Bosnien und Herzegowina</option>
                                <option value="BW">Botswana</option>
                                <option value="BV">Bouvetinsel</option>
                                <option value="BR">Brasilien</option>
                                <option value="VG">Britische Jungferninseln</option>
                                <option value="IO">Britisches Territorium im Indischen Ozean</option>
                                <option value="BN">Brunei Darussalam</option>
                                <option value="BG">Bulgarien</option>
                                <option value="BF">Burkina Faso</option>
                                <option value="BI">Burundi</option>
                            </optgroup>
                            <optgroup label="C">
                                <option value="EA">Ceuta, Melilla</option>
                                <option value="CL">Chile</option>
                                <option value="CN">Volksrepublik China</option>
                                <option value="CP">Clipperton</option>
                                <option value="CK">Cookinseln</option>
                                <option value="CR">Costa Rica</option>
                                <option value="CI">Côte d'Ivoire (Elfenbeinküste)</option>
                            </optgroup>
                            <optgroup label="D">
                                <option value="DK">Dänemark</option>
                                <option value="DE">Deutschland</option>
                                <option value="DG">Diego Garcia</option>
                                <option value="DM">Dominica</option>
                                <option value="DO">Dominikanische Republik</option>
                                <option value="DJ">Dschibuti</option>
                            </optgroup>
                            <optgroup label="E">
                                <option value="EC">Ecuador</option>
                                <option value="SV">El Salvador</option>
                                <option value="ER">Eritrea</option>
                                <option value="EE">Estland</option>
                            </optgroup>
                            <optgroup label="F">
                                <option value="FK">Falklandinseln</option>
                                <option value="FO">Färöer</option>
                                <option value="FJ">Fidschi</option>
                                <option value="FI">Finnland</option>
                                <option value="FR">Frankreich</option>
                                <option value="GF">Französisch-Guayana</option>
                                <option value="PF">Französisch-Polynesien</option>
                            </optgroup>
                            <optgroup label="G">
                                <option value="GA">Gabun</option>
                                <option value="GM">Gambia</option>
                                <option value="GE">Georgien</option>
                                <option value="GH">Ghana</option>
                                <option value="GI">Gibraltar</option>
                                <option value="GD">Grenada</option>
                                <option value="GR">Griechenland</option>
                                <option value="GL">Grönland</option>
                                <option value="GB">Grossbritannien</option>
                                <option value="GP">Guadeloupe</option>
                                <option value="GU">Guam</option>
                                <option value="GT">Guatemala</option>
                                <option value="GG">Guernsey (Kanalinsel)</option>
                                <option value="GN">Guinea</option>
                                <option value="GW">Guinea-Bissau</option>
                                <option value="GY">Guyana</option>
                            </optgroup>
                            <optgroup label="H">
                                <option value="HT">Haiti</option>
                                <option value="HM">Heard- und McDonald-Inseln</option>
                                <option value="HN">Honduras</option>
                                <option value="HK">Hongkong</option>
                            </optgroup>
                            <optgroup label="I">
                                <option value="IN">Indien</option>
                                <option value="ID">Indonesien</option>
                                <option value="IM">Insel Man</option>
                                <option value="IQ">Irak</option>
                                <option value="IR">Iran</option>
                                <option value="IE">Irland</option>
                                <option value="IS">Island</option>
                                <option value="IL">Israel</option>
                                <option value="IT">Italien</option>
                            </optgroup>
                            <optgroup label="J">
                                <option value="JM">Jamaika</option>
                                <option value="JP">Japan</option>
                                <option value="YE">Jemen</option>
                                <option value="JE">Jersey (Kanalinsel)</option>
                                <option value="JO">Jordanien</option>
                            </optgroup>
                            <optgroup label="K">
                                <option value="KY">Kaimaninseln</option>
                                <option value="KH">Kambodscha</option>
                                <option value="CM">Kamerun</option>
                                <option value="CA">Kanada</option>
                                <option value="IC">Kanarische Inseln</option>
                                <option value="CV">Kap Verde</option>
                                <option value="KZ">Kasachstan</option>
                                <option value="QA">Katar</option>
                                <option value="KE">Kenia</option>
                                <option value="KG">Kirgisistan</option>
                                <option value="KI">Kiribati</option>
                                <option value="CC">Kokosinseln</option>
                                <option value="CO">Kolumbien</option>
                                <option value="KM">Komoren</option>
                                <option value="CD">Demokratische Republik Kongo</option>
                                <option value="KP">Demokratische Volksrepublik Korea (Nordkorea)</option>
                                <option value="KR">Republik Korea (Südkorea)</option>
                                <option value="HR">Kroatien</option>
                                <option value="CU">Kuba</option>
                                <option value="KW">Kuwait</option>
                            </optgroup>
                            <optgroup label="L">
                                <option value="LA">Laos</option>
                                <option value="LS">Lesotho</option>
                                <option value="LV">Lettland</option>
                                <option value="LB">Libanon</option>
                                <option value="LR">Liberia</option>
                                <option value="LY">Libyen</option>
                                <option value="LI">Liechtenstein</option>
                                <option value="LT">Litauen</option>
                                <option value="LU">Luxemburg</option>
                            </optgroup>
                            <optgroup label="M">
                                <option value="MO">Macao</option>
                                <option value="MG">Madagaskar</option>
                                <option value="MW">Malawi</option>
                                <option value="MY">Malaysia</option>
                                <option value="MV">Malediven</option>
                                <option value="ML">Mali</option>
                                <option value="MT">Malta</option>
                                <option value="MA">Marokko</option>
                                <option value="MH">Marshallinseln</option>
                                <option value="MQ">Martinique</option>
                                <option value="MR">Mauretanien</option>
                                <option value="MU">Mauritius</option>
                                <option value="YT">Mayotte</option>
                                <option value="MK">Mazedonien</option>
                                <option value="MX">Mexiko</option>
                                <option value="FM">Mikronesien</option>
                                <option value="MD">Moldawien (Republik Moldau)</option>
                                <option value="MC">Monaco</option>
                                <option value="MN">Mongolei</option>
                                <option value="ME">Montenegro</option>
                                <option value="MS">Montserrat</option>
                                <option value="MZ">Mosambik</option>
                                <option value="MM">Myanmar (Burma)</option>
                            </optgroup>
                            <optgroup label="N">
                                <option value="NA">Namibia</option>
                                <option value="NR">Nauru</option>
                                <option value="NP">Nepal</option>
                                <option value="NC">Neukaledonien</option>
                                <option value="NZ">Neuseeland</option>
                                <option value="NI">Nicaragua</option>
                                <option value="NL">Niederlande</option>
                                <option value="AN">Niederländische Antillen</option>
                                <option value="NE">Niger</option>
                                <option value="NG">Nigeria</option>
                                <option value="NU">Niue</option>
                                <option value="MP">Nördliche Marianen</option>
                                <option value="NF">Norfolkinsel</option>
                                <option value="NO">Norwegen</option>
                            </optgroup>
                            <optgroup label="O">
                                <option value="OM">Oman</option>
                                <option value="XO">Orbit</option>
                                <option value="AT">Österreich</option>
                                <option value="TL">Osttimor (Timor-Leste)</option>
                            </optgroup>
                            <optgroup label="P">
                                <option value="PK">Pakistan</option>
                                <option value="PS">Palästinensische Autonomiegebiete</option>
                                <option value="PW">Palau</option>
                                <option value="PA">Panama</option>
                                <option value="PG">Papua-Neuguinea</option>
                                <option value="PY">Paraguay</option>
                                <option value="PE">Peru</option>
                                <option value="PH">Philippinen</option>
                                <option value="PN">Pitcairninseln</option>
                                <option value="PL">Polen</option>
                                <option value="PT">Portugal</option>
                                <option value="PR">Puerto Rico</option>
                            </optgroup>
                            <optgroup label="Q">
                                </option>
                            </optgroup>
                            <optgroup label="R">
                                <option value="TW">Republik China (Taiwan)</option>
                                <option value="CG">Republik Kongo</option>
                                <option value="RE">Réunion</option>
                                <option value="RW">Ruanda</option>
                                <option value="RO">Rumänien</option>
                                <option value="RU">Russische Föderation</option>
                            </optgroup>
                            <optgroup label="S">
                                <option value="BL">Saint-Barthélemy</option>
                                <option value="MF">Saint-Martin</option>
                                <option value="SB">Salomonen</option>
                                <option value="ZM">Sambia</option>
                                <option value="WS">Samoa</option>
                                <option value="SM">San Marino</option>
                                <option value="ST">São Tomé und Príncipe</option>
                                <option value="SA">Saudi-Arabien</option>
                                <option value="SE">Schweden</option>
                                <option value="CH">Schweiz</option>
                                <option value="SN">Senegal</option>
                                <option value="RS">Serbien</option>
                                <option value="SC">Seychellen</option>
                                <option value="SL">Sierra Leone</option>
                                <option value="ZW">Simbabwe</option>
                                <option value="SG">Singapur</option>
                                <option value="SK">Slowakei</option>
                                <option value="SI">Slowenien</option>
                                <option value="SO">Somalia</option>
                                <option value="ES">Spanien</option>
                                <option value="LK">Sri Lanka</option>
                                <option value="SH">St. Helena</option>
                                <option value="KN">St. Kitts und Nevis</option>
                                <option value="LC">St. Lucia</option>
                                <option value="PM">Saint-Pierre und Miquelon</option>
                                <option value="VC">St. Vincent und die Grenadinen</option>
                                <option value="ZA">Südafrika</option>
                                <option value="SD">Sudan</option>
                                <option value="GS">Südgeorgien und die Südlichen Sandwichinseln</option>
                                <option value="SR">Suriname</option>
                                <option value="SJ">Svalbard und Jan Mayen</option>
                                <option value="SZ">Swasiland</option>
                                <option value="SY">Syrien</option>
                            </optgroup>
                            <optgroup label="T">
                                <option value="TJ">Tadschikistan</option>
                                <option value="TZ">Tansania</option>
                                <option value="TH">Thailand</option>
                                <option value="TG">Togo</option>
                                <option value="TK">Tokelau</option>
                                <option value="TO">Tonga</option>
                                <option value="TT">Trinidad und Tobago</option>
                                <option value="TA">Tristan da Cunha</option>
                                <option value="TD">Tschad</option>
                                <option value="CZ">Tschechische Republik</option>
                                <option value="TN">Tunesien</option>
                                <option value="TR">Türkei</option>
                                <option value="TM">Turkmenistan</option>
                                <option value="TC">Turks- und Caicosinseln</option>
                                <option value="TV">Tuvalu</option>
                            </optgroup>
                            <optgroup label="U">
                                <option value="UG">Uganda</option>
                                <option value="UA">Ukraine</option>
                                <option value="HU">Ungarn</option>
                                <option value="UY">Uruguay</option>
                                <option value="UZ">Usbekistan</option>
                            </optgroup>
                            <optgroup label="V">
                                <option value="VU">Vanuatu</option>
                                <option value="VA">Vatikanstadt</option>
                                <option value="VE">Venezuela</option>
                                <option value="AE">Vereinigte Arabische Emirate</option>
                                <option value="US">Vereinigte Staaten von Amerika (USA)</option>
                                <option value="VN">Vietnam</option>
                            </optgroup>
                            <optgroup label="W">
                                <option value="WF">Wallis und Futuna</option>
                                <option value="CX">Weihnachtsinsel</option>
                                <option value="EH">Westsahara</option>
                            </optgroup>
                            <optgroup label="Z">
                                <option value="CF">Zentralafrikanische Republik</option>
                                <option value="CY">Zypern</option>
                            </optgroup>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="style" class="col-sm-2 col-form-label">Stil</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control ui-widget-content ui-corner-all" id="style" name="style">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="abv" class="col-sm-2 col-form-label">Alkohol</label>
                    <div class="col-sm-10">
                        <input type="number" min="0.0" max="99.9" step="0.1" class="form-control ui-widget-content ui-corner-all" id="abv" name="abv">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="spezial" class="col-sm-2 col-form-label">Spezial</label>
                    <div class="form-check form-check-inline col-sm-7">
                        <input class="form-check-input" type="checkbox" id="tag_ba" value="on" name="tag_ba">
                        <label class="col-form-label" for="tag_ba">Barrel</label>&nbsp;&nbsp;
                        <input class="form-check-input" type="checkbox" id="tag_bret" value="on" name="tag_bret">
                        <label class="col-form-label" for="tag_bret">Brett</label>&nbsp;&nbsp;
                        <input class="form-check-input" type="checkbox" id="tag_brut" value="on" name="tag_brut">
                        <label class="col-form-label" for="tag_brut">Brut</label>&nbsp;&nbsp;
                        <input class="form-check-input" type="checkbox" id="tag_fruit" value="on" name="tag_fruit">
                        <label class="col-form-label" for="tag_fruit">Fruited</label>&nbsp;&nbsp;
                        <input class="form-check-input" type="checkbox" id="tag_pastry" value="on" name="tag_pastry">
                        <label class="col-form-label" for="tag_pastry">Pastry</label>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="info" class="col-sm-2 col-form-label">Info</label>
                    <div class="col-sm-10">
                        <textarea class="form-control ui-widget-content ui-corner-all" name="info" id="info" rows="3"></textarea>
                        <small class="form-text text-muted"> Beschreiben Sie besonderheiten Ihres Biers. Dies ist auch der richtige Platz um weitere Zutaten zu erwähnen.</small>
                    </div>
                </div>
            </div>
        <div class="col-sm-4">
                <legend class="col-form-label col-sm-12 pt-0">Lager Informationen</legend>
                <div class="form-group row">
                    <label for="amount" class="col-sm-3 col-form-label">Anzahl</label>
                    <div class="col-sm-9">
                        <input type="number" min="1" max="99" step="1" class="form-control ui-widget-content ui-corner-all" id="amount" name="amount">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="bestbefore" class="col-sm-3 col-form-label">Ablauf</label>
                    <div class="col-sm-9">
                        <input type="date" class="form-control ui-widget-content ui-corner-all" id="bestbefore" value="{{datebest date}}" name="bestbefore">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="container" class="col-sm-3 col-form-label">Gebinde</label>
                    <div class="col-sm-9">
                        <select id="container" class="form-control ui-widget-content ui-corner-all" name="container">
                            {{#each data.containers}}
                            <option value="{{id}}">{{volume}}l {{container}}</option>
                            {{/each}}
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="storage" class="col-sm-3 col-form-label">Lagerort</label>
                    <div class="col-sm-9">
                        <select class="form-control ui-widget-content ui-corner-all" id="storage" name="storage">
                            {{#each data.storage}}
                            <option value="{{id}}">{{location}}</option>
                            {{/each}}
                        </select>
                    </div>
                </div>
            <div class="action-buttons">
            <button type="button" data-action="stock_insert" class="ui-button ui-widget ui-corner-all  btn-lg">Speichern</button>
            </div>
        </div>
    </form>
</script>
<script id="template_stock" type="text/x-handlebars-template">
    <table class="stock">
        <tr>
            <th>Brauerei</th>
            <th>Land</th>
            <th>Bier</th>
            <th>Stil</th>
            <th>abv%</th>
        </tr>
    </table>
    <table class="stock">
    {{#each data}}
        <tr data-beerid="{{beerid}}">
            <td>{{brewery}}</td>
            <td>{{country}}</td>
            <td>{{beer}}</td>
            <td>{{style}}</td>
            <td>{{abv}}</td>
        </tr>
    {{/each}}
    </table>
    <div id="output2"></div>
    <div id="dialog-form" title="Bierlager filtern" class="ui-front">
        <form>
            <div class="col-sm-6">
                <fieldset class="col-sm-12 form-group">
                    <div class="form-group row">
                        <label for="filter-brewery" class="col-sm-4 col-form-label">Brauerei</label>
                        <div class="col-sm-8"><input type="text" name="filter-brewery" id="filter-brewery" value="" class="text ui-widget-content ui-corner-all"></div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-4"><label for="filter-country" class="col-form-label">Land /</label><label for="filter-style" class="col-form-label"> Stil</label></div>
                        <div class="col-sm-8"><input type="text" name="filter-country" id="filter-country" value="" class="text ui-widget-content ui-corner-all"><input type="text" name="filter-style" id="filter-style" value="" class="text ui-widget-content ui-corner-all"></div>
                    </div>
                    <div class="form-group row">
                        <label for="filter-description" class="col-sm-4 col-form-label">Beschreibung</label>
                        <div class="col-sm-8"><textarea name="filter-description" id="filter-description" value="" class="text ui-widget-content ui-corner-all" /></div>
                    </div>
                </fieldset>
            </div>
            <div class="col-sm-6">
                <fieldset class="col-sm-12 form-group">
                    <div class="form-group row">
                        <div class="col-sm-4">
                            <div class="form-check form-check-inline">
                                <label for="filter-beer" class="col-form-label">Bier</label>
                                <input class="form-check-input" type="checkbox" id="show_outofstock" checked="checked" value="on" name="show_outofstock">
                                <label class="col-form-label" for="show_outofstock">Stock</label>
                            </div>
                        </div>
                        <div class="col-sm-8"><input type="text" name="filter-beer" id="filter-beer" value="" class="text ui-widget-content ui-corner-all"></div>
                    </div>
                    <div class="form-group row">
                        <label for="filter-createdfrom" class="col-sm-4 col-form-label">Eingelagert</label>
                        <div class="col-sm-4"><input type="date" name="filter-createdfrom" id="filter-createdfrom" value="" class="text ui-widget-content ui-corner-all"></div>
                        <div class="col-sm-4"><input type="date" name="filter-ceatedto" id="filter-createdto" value="" class="text ui-widget-content ui-corner-all"></div>
                    </div>
                    <div class="form-group row">
                        <label for="filter-drunkenfrom" class="col-sm-4 col-form-label">Ausgelagert</label>
                        <div class="col-sm-4"><input type="date" name="filter-drunkenfrom" id="filter-drunkenfrom" value="" class="text ui-widget-content ui-corner-all"></div>
                        <div class="col-sm-4"><input type="date" name="filter-drunkento" id="filter-drunkento" value="" class="text ui-widget-content ui-corner-all"></div>
                    </div>
                    <div class="form-group row">
                        <div class="form-check form-check-inline col-sm-12">
                            <input class="form-check-input" type="checkbox" id="filter_tag_ba" value="on" name="filter_tag_ba">
                            <label class="col-form-label" for="filter_tag_ba">Barrel</label>&nbsp;&nbsp;&nbsp;&nbsp;
                            <input class="form-check-input" type="checkbox" id="filter_tag_bret" value="on" name="filter_tag_bret">
                            <label class="col-form-label" for="filter_tag_bret">Brett</label>&nbsp;&nbsp;&nbsp;&nbsp;
                            <input class="form-check-input" type="checkbox" id="filter_tag_brut" value="on" name="filter_tag_brut">
                            <label class="col-form-label" for="filter_tag_brut">Brut</label>&nbsp;&nbsp;&nbsp;&nbsp;
                            <input class="form-check-input" type="checkbox" id="filter_tag_fruit" value="on" name="filter_tag_fruit">
                            <label class="col-form-label" for="filter_tag_fruit">Fruited</label>&nbsp;&nbsp;&nbsp;&nbsp;
                            <input class="form-check-input" type="checkbox" id="filter_tag_pastry" value="on" name="filter_tag_pastry">
                            <label class="col-form-label" for="filter_tag_pastry">Pastry</label>
                        </div>
                    </div>
                </fieldset>
            </div>
            <div class="col-sm-12">
                <fieldset class="col-sm-12 form-group">
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label" for="filter-abv-slider">Alkohol vol.%</label>
                        <div class="col-sm-10">
                            <div id="filter-abv-slider" class="form-check-input"></div>
                        </div>
                    </div>
                </fieldset>
            </div>
        </form>
    </div>
</script>
<script id="template_stock_info" type="text/x-handlebars-template">
    <table class="stock_info">
        <tr>
            <th colspan="5">Informationen zum Bier:</th>
        </tr>
        <tr>
            <td colspan="5">{{#each data}}{{#if @first}}{{#if info}}{{info}}{{else}}&nbsp;<br>{{/if}}{{/if}}{{/each}}</td>
        </tr>
        <tr>
        	<th>Barrel</th>
        	<th>Brett</th>
        	<th>Brut</th>
        	<th>Frucht</th>
        	<th>Pastry</th>
        </tr>
        {{#each data}}{{#if @first}}
        <tr>
        	<td>{{#if tag_ba}}✓{{else}}&nbsp;{{/if}}</td>
        	<td>{{#if tag_bret}}✓{{else}}&nbsp;{{/if}}</td>
        	<td>{{#if tag_brut}}✓{{else}}&nbsp;{{/if}}</td>
        	<td>{{#if tag_fruit}}✓{{else}}&nbsp;{{/if}}</td>
        	<td>{{#if tag_pastry}}✓{{else}}&nbsp;{{/if}}</td>
        </tr>
        {{/if}}{{/each}}
    </table>
    <table class="stock_info">
        <tr>
            <th>Gebinde</th>
            <th>Ablauf</th>
            <th>Lagerort</th>
        </tr>
    </table>
    <table class="stock_info">
        {{#each data}}
        <tr>
            <td>{{volume}}l {{container}}</td>
            <td>{{date best}}</td>
            <td>{{location}}</td>
        </tr>
        {{/each}}
    </table>
</script>
<script id="template_qrcode" type="text/x-handlebars-template">
    <h1>Was soll mit diesem Bier geschehen?</h1>
        {{#each data}}
            <h1 data-id="{{stockid}}">{{brewery}} ({{country}}) - {{beer}}</h1>
            <p>{{info}}</p>
            <div>◆ {{abv}}%Alc. ◆ {{style}} ◆ {{#if tag_ba}}Barrel Aged ◆ {{/if}}{{#if tag_bret}}Brett ◆ {{/if}}{{#if tag_brut}}Brut ◆  {{/if}}{{#if tag_fruit}}Fruit ◆ {{/if}}{{#if tag_pastry}}Pastry ◆ {{/if}}</div>
            <div>◆ {{volume}}l {{container}} ◆ {{date best}} ◆ {{location}} ◆ </div>
            <div class="action-buttons">
        {{#if on_stock}}
        <button type="button" data-action="stock_checkout" class="ui-button ui-widget ui-corner-all btn-lg">Jetzt trinken!</button>
        <button type="button" data-action="stock_change" class="ui-button ui-widget ui-corner-all btn-lg">Lagerort ändern</button>
        {{else}}
        <button type="button" data-action="stock_restock" class="ui-button ui-widget ui-corner-all btn-lg">Erneut einlagern.</button>
        {{/if}}
        <button type="button" data-action="cancel" class="ui-button ui-widget ui-corner-all btn-lg">Nichts.</button>
        {{/each}}
        </div>
</script>
<script id="template_stock_change" type="text/x-handlebars-template">
    <h1>Lagerort ändern:</h1>
        {{#each data.data}}
            <h1 data-id="{{stockid}}">{{brewery}} ({{country}}) - {{beer}}</h1>
            <p>{{info}}</p>
            <div>◆ {{abv}}%Alc. ◆ {{style}} ◆ {{#if tag_ba}}Barrel Aged ◆ {{/if}}{{#if tag_bret}}Brett ◆ {{/if}}{{#if tag_brut}}Brut ◆  {{/if}}{{#if tag_fruit}}Fruit ◆ {{/if}}{{#if tag_pastry}}Pastry ◆ {{/if}}</div>
            <div>◆ {{volume}}l {{container}} ◆ {{date best}} ◆ {{location}} ◆ </div>
            <div>
                <form>
                <select id="change_storage" name="storage" class="form-control">
            {{#each ../data.storage}}
                <option value="{{id}}">{{location}}</option>
            {{/each}}
            </select></form></div>
            <div class="action-buttons">
        <button type="button" data-action="stock_change_save" class="ui-button ui-widget ui-corner-all btn-lg">Lagerort ändern</button>
        <button type="button" data-action="cancel" class="ui-button ui-widget ui-corner-all btn-lg">Nichts.</button>
        {{/each}}
        </div>
</script>
<script id="template_stats" type="text/x-handlebars-template">
    <div class="stats">
    <div>
    <h2>Anzahl Biere je Stil:</h2>
    <table>
    {{#each data.styles}}
        <tr>
            <td>{{style}}</td>
            <td>{{anzahl}}</td>
        </tr>
    {{/each}}
    {{#each data.beertotal}}
        <tr>
            <td>Total</td>
            <td>{{anzahl}}</td>
        </tr>
    {{/each}}
    </table>
    </div>
    <div>
    <h2>Neu hinzugefügt:</h2>
    <table>
    {{#each data.fresh}}
        <tr>
            <td>{{beer}}</td>
            <td>{{brewery}} ({{country}})</td>
            <td>{{style}}</td>
        </tr>
    {{/each}}
    </table>
    </div>
    <div>
    <h2>Drink NOW!</h2>
    <table>
    {{#each data.drinknow}}
        <tr>
            <td>{{beer}}</td>
            <td>{{brewery}} ({{country}})</td>
            <td>{{style}}</td>
        </tr>
    {{/each}}
    </table></div>
    </div>
</script>
</body>
