#!/usr/bin/python3 -u

# import config dict
from config import cfg

# change working dir
import os
os.chdir("/opt/beerventory/")

# imports for websockets
import asyncio
import json
import websockets

# imports for qrcode scanner
from time import sleep
import evdev
from evdev import InputDevice, categorize, ecodes

# imports for printing barcodes
import cups
import qrcode
from PIL import Image
from PIL import ImageFont, ImageDraw, ImageOps

# imports for database
import sqlite3
# define a new dict factory so that results from sqlite are in JSON form
def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d

def print_qr_code(data):

    background = Image.new("RGBA", (306, 306), (255, 255, 255, 255))
    layer1 = Image.new("RGBA", (200, 40), (255, 255, 255, 255))
    layer2 = Image.new("RGBA", (200, 40), (255, 255, 255, 255))
    layer3 = Image.new("RGBA", (200, 200), (255, 255, 255, 255))

    font = ImageFont.load_default()
    font_size = 30
    font = ImageFont.truetype("./SourceCodePro-Bold.ttf", font_size)

    draw1 = ImageDraw.Draw(layer1)
    draw1.text((2, 2), "beerventory", fill="black", font=font)

    draw2 = ImageDraw.Draw(layer2)
    draw2.text((2, 2), str(data['best']), fill="black", font=font)

    draw3 = ImageDraw.Draw(layer3)
    draw3.text((2, 2), str(data['stockid']), fill="black", font=font)

    layer1_rot = layer1.rotate(0, expand=False, fillcolor="white")
    layer2_rot = layer2.rotate(0, expand=False, fillcolor="white")
    layer3_rot = layer3.rotate(90, expand=False, fillcolor="white")
    layer3_crop = layer3_rot.crop((0,0,40,200))
    background.paste( layer1_rot, (10,0) )
    background.paste( layer2_rot, (100,255) )
    background.paste( layer3_crop, (260,0) )


    qr = qrcode.QRCode(
        box_size=4,
        version=8,
        border=2,
        error_correction=qrcode.constants.ERROR_CORRECT_H
    )
    qr.add_data(str(data['stockid']))
    qr.make()
    qr_img = qr.make_image().convert('RGBA')

    background.paste(qr_img, (48,48))
    background.save('./qrcode.png')

    conn = cups.Connection()
    conn.printFile(cfg['PRINTER'],'./qrcode.png',"",{})

async def get_stock_insert(ws):
    results = {};
    db.execute('''SELECT id,brewery,country FROM breweries ORDER BY brewery;''')
    results['breweries'] = db.fetchall()
    db.execute('''SELECT id,beer,brewery,style,abv,tag_ba,tag_brut,tag_bret,tag_fruit,tag_pastry,info FROM beers ORDER BY beer;''')
    results['beers'] = db.fetchall()
    db.execute('''SELECT id,style FROM styles ORDER BY style;''')
    results['styles'] = db.fetchall()
    db.execute('''SELECT id,location FROM storage ORDER BY id;''')
    results['storage'] = db.fetchall()
    db.execute('''SELECT id, volume, container FROM containers ORDER BY id;''')
    results['containers'] = db.fetchall()

    loop.create_task(ws.send(json.dumps({"type": "stock_insert", "data": results})))

async def get_stock_insert_save(ws,data):
    b = {}
    for d in data:
        b[d['name']] = d['value']

    db.execute('''INSERT OR IGNORE INTO breweries (brewery,country) VALUES (?,?);''',(b['brewery'],b['country']))
    db.execute('''SELECT id FROM breweries WHERE brewery = ? AND country = ?;''',(b['brewery'],b['country']))
    brewery_id = db.fetchone()

    db.execute('''INSERT OR IGNORE INTO styles (style) VALUES (?);''',(b['style'],))
    db.execute('''SELECT id FROM styles WHERE style = ?;''',(b['style'],))
    style_id = db.fetchone()

    if not 'tag_ba' in b:
        b['tag_ba'] = '';
    if not 'tag_bret' in b:
        b['tag_bret'] = '';
    if not 'tag_brut' in b:
        b['tag_brut'] = '';
    if not 'tag_fruit' in b:
        b['tag_fruit'] = '';
    if not 'tag_pastry' in b:
        b['tag_pastry'] = '';

    db.execute('''INSERT OR IGNORE INTO beers (beer,brewery,style,abv,tag_ba,tag_bret,tag_brut,tag_fruit,tag_pastry,info) VALUES (?,?,?,?,?,?,?,?,?,?);''',(b['name'],brewery_id['id'],style_id['id'],b['abv'],b['tag_ba'],b['tag_bret'],b['tag_brut'],b['tag_fruit'],b['tag_pastry'],b['info']))
    db.execute('''SELECT id FROM beers WHERE beer = ?;''',(b['name'],))
    beer_id = db.fetchone()

    for i in list(range(int(b['amount']))):
        db.execute('''INSERT INTO stock (beer,best,container,storage,on_stock,date_added) VALUES (?,?,?,?,?,date('now'));''',(beer_id['id'],b['bestbefore'],b['container'],b['storage'],1))
        print_qr_code({'best': b['bestbefore'], 'stockid': db.lastrowid})

    conn.commit()

async def get_stock_restock(ws,data):
    db.execute('''UPDATE stock SET on_stock = '1', date_removed = '' WHERE id = ?;''',(data,))
    conn.commit()
    await get_stock(ws)

async def get_stock_checkout(ws,data):
    db.execute('''UPDATE stock SET on_stock = '0', date_removed = date('now') WHERE id = ?;''',(data,))
    conn.commit()
    await get_stock(ws)

async def get_stock_change(ws,data):
    results = {};
    db.execute('''SELECT id,location FROM storage ORDER BY id;''')
    results['storage'] = db.fetchall()
    db.execute('''SELECT
breweries.brewery,
breweries.country,
beers.beer,
styles.style,
beers.abv,
beers.info,
beers.tag_ba,
beers.tag_brut,
beers.tag_bret,
beers.tag_fruit,
beers.tag_pastry,
stock.best,
containers.container,
containers.volume,
storage.location,
stock.id as stockid
FROM beers
JOIN breweries ON (beers.brewery = breweries.id)
JOIN stock ON (stock.beer = beers.id)
JOIN styles ON (beers.style = styles.id)
JOIN storage ON (stock.storage = storage.id)
JOIN containers ON (stock.container = containers.id)
WHERE stock.id = ?
ORDER BY breweries.brewery, beers.beer
;''',(data,))

    results['data'] = db.fetchall()
    loop.create_task(ws.send(json.dumps({"type": "stock_change", "data": results})))

async def get_stock_change_save(ws,data):
    db.execute('''UPDATE stock SET storage = ? WHERE id = ?;''',(data[0],data[1]))
    conn.commit()
    await get_stock(ws)

async def get_stock(ws):
    db.execute('''SELECT
    breweries.brewery,
    breweries.country,
    beers.beer,
    styles.style,
    beers.abv,
    beers.id as beerid
FROM beers
JOIN breweries ON (beers.brewery = breweries.id)
JOIN stock ON (stock.beer = beers.id)
JOIN styles ON (beers.style = styles.id)
WHERE stock.on_stock = 1
GROUP BY breweries.brewery, beers.beer
ORDER BY breweries.brewery, beers.beer
;''')
    results = db.fetchall()
    loop.create_task(ws.send(json.dumps({"type": "stock", "data": results})))

async def get_stock_info(ws,data):
    db.execute('''SELECT
beers.info,
beers.tag_ba,
beers.tag_brut,
beers.tag_bret,
beers.tag_fruit,
beers.tag_pastry,
stock.best,
containers.container,
containers.volume,
storage.location
FROM beers
JOIN breweries ON (beers.brewery = breweries.id)
JOIN stock ON (stock.beer = beers.id)
JOIN styles ON (beers.style = styles.id)
JOIN storage ON (stock.storage = storage.id)
JOIN containers ON (stock.container = containers.id)
WHERE beers.id = ?
ORDER BY breweries.brewery, beers.beer
;''',(data,))

    results = db.fetchall()
    loop.create_task(ws.send(json.dumps({"type": "stock_info", "data": results})))

async def get_stock_dialog_filter(ws):
    results = {};
    db.execute('''SELECT id,brewery || ' (' || country || ')' AS brewery FROM breweries ORDER BY brewery;''')
    results['breweries'] = db.fetchall()
    db.execute('''SELECT id,beer,brewery,style,abv,tag_ba,tag_brut,tag_bret,tag_fruit,tag_pastry,info FROM beers ORDER BY beer;''')
    results['beers'] = db.fetchall()
    db.execute('''SELECT id,style FROM styles ORDER BY style;''')
    results['styles'] = db.fetchall()
    db.execute('''SELECT DISTINCT country FROM breweries ORDER BY country;''')
    results['countries'] = db.fetchall()

    loop.create_task(ws.send(json.dumps({"type": "stock_dialog_filter", "data": results})))

async def get_stats(ws):
    results = {};
    # beer count per style
    db.execute('''SELECT
styles.style,
COUNT(beers.beer) as anzahl
FROM beers
JOIN breweries ON (beers.brewery = breweries.id)
JOIN stock ON (stock.beer = beers.id)
JOIN styles ON (beers.style = styles.id)
JOIN storage ON (stock.storage = storage.id)
JOIN containers ON (stock.container = containers.id)
WHERE stock.on_stock = 1
GROUP BY styles.style
ORDER BY styles.style;
''')
    results['styles'] = db.fetchall()

# total beers
    db.execute('''SELECT
COUNT(beers.beer) as anzahl
FROM beers
JOIN stock ON (stock.beer = beers.id)
WHERE stock.on_stock = 1;
''')
    results['beertotal'] = db.fetchall()

# 15 fresh added beers in the last 10 days
    db.execute('''SELECT
beers.beer,
breweries.brewery,
breweries.country,
styles.style
FROM beers
JOIN breweries ON (beers.brewery = breweries.id)
JOIN stock ON (stock.beer = beers.id)
JOIN styles ON (beers.style = styles.id)
JOIN storage ON (stock.storage = storage.id)
JOIN containers ON (stock.container = containers.id)
WHERE stock.on_stock = 1
AND stock.date_added > date(strftime('%s','now')-(3600*24*10),'unixepoch')
GROUP BY beers.beer
ORDER BY beers.beer
LIMIT 15;
''')
    results['fresh'] = db.fetchall()

# oldest 15 beers => DRINK NOW!
    db.execute('''SELECT
beers.beer,
breweries.brewery,
breweries.country,
styles.style,
stock.best
FROM beers
JOIN breweries ON (beers.brewery = breweries.id)
JOIN stock ON (stock.beer = beers.id)
JOIN styles ON (beers.style = styles.id)
JOIN storage ON (stock.storage = storage.id)
JOIN containers ON (stock.container = containers.id)
WHERE stock.on_stock = 1
GROUP BY beers.beer
ORDER BY stock.best, beers.beer
LIMIT 15;
''')
    results['drinknow'] = db.fetchall()

    loop.create_task(ws.send(json.dumps({"type": "stats", "data": results})))

async def send_qr(code):
    if CLIENTS:  # asyncio.wait doesn't accept an empty list
        db.execute('''SELECT
breweries.brewery,
breweries.country,
beers.beer,
beers.info,
styles.style,
beers.abv,
beers.tag_ba,
beers.tag_brut,
beers.tag_bret,
beers.tag_fruit,
beers.tag_pastry,
stock.id as stockid,
stock.best,
stock.on_stock,
containers.container,
containers.volume,
storage.location
FROM beers
JOIN breweries ON (beers.brewery = breweries.id)
JOIN stock ON (stock.beer = beers.id)
JOIN styles ON (beers.style = styles.id)
JOIN storage ON (stock.storage = storage.id)
JOIN containers ON (stock.container = containers.id)
WHERE stockid = ?
ORDER BY breweries.brewery, beers.beer
;''',(code,))

        results = db.fetchall()

        message = json.dumps({"type": "qrcode", "data": results})
        await asyncio.wait([client.send(message) for client in CLIENTS])

async def register(websocket):
    CLIENTS.add(websocket)

async def unregister(websocket):
    CLIENTS.remove(websocket)

async def ws_server(websocket, path):
    # register(websocket) registers a new session in the CLIENTS dict
    await register(websocket)
    try:
        async for message in websocket:
            data = json.loads(message)
            if data["action"] == "stock":
                await get_stock(websocket)
            elif data["action"] == "stock_info":
                await get_stock_info(websocket,data["data"])
            elif data["action"] == "stock_dialog_filter":
                await get_stock_dialog_filter(websocket)
            elif data["action"] == "stock_restock":
                await get_stock_restock(websocket,data["data"])
            elif data["action"] == "stock_checkout":
                await get_stock_checkout(websocket,data["data"])
            elif data["action"] == "stock_change":
                await get_stock_change(websocket,data["data"])
            elif data["action"] == "stock_change_save":
                await get_stock_change_save(websocket,data["data"])
            elif data["action"] == "stock_insert":
                await get_stock_insert(websocket)
            elif data["action"] == "stock_insert_save":
                await get_stock_insert_save(websocket,data["data"])
            elif data["action"] == "stats":
                await get_stats(websocket)
            else:
                print("unsupported event:")
                print(data)
    finally:
        await unregister(websocket)

async def get_qr_scanner_code():

    #setup vars
    x = ''
    caps = False
    #loop
    async for event in device.async_read_loop():
        if event.type == ecodes.EV_KEY:
            data = categorize(event)  # Save the event temporarily to introspect it
            # if shift is pressed for CAPS
            if data.scancode == 42 or data.scancode == 54:
                if data.keystate == 1:
                    caps = True
                if data.keystate == 0:
                    caps = False
            if data.keystate == 1:  # Down events only
                if caps:
                    key_lookup = u'{}'.format(capscodes.get(data.scancode)) or data.scancode
                else:
                    key_lookup = u'{}'.format(scancodes.get(data.scancode)) or data.scancode
                # if key pressed is not shift nor enter, add the looked up key to the output string
                if (data.scancode != 42) and (data.scancode != 54) and (data.scancode != 28):
                    x += key_lookup
                # if we have a enter the input is finished and we send the complete string to webapp
                if(data.scancode == 28):
                    # print(x)          # Print it all out!
                    await send_qr(x)
                    x = ''            # reset the output string



device = '';
while True:
    # Find all input devices.
    devices = [evdev.InputDevice(fn) for fn in evdev.list_devices()]
    # Limit the list to those containing MATCH and pick the first one.
    for d in devices:
        if cfg['SCANNER'] in d.name:
            device = d
    if device != '' and os.path.exists(device.path):
        break
    sleep(1)

# Grab, i.e. prevent the keyboard from emitting original events.
device.grab()

scancodes = {
    2: u'1', 3: u'2', 4: u'3', 5: u'4', 6: u'5', 7: u'6', 8: u'7', 9: u'8', 10: u'9', 11: u'0', 12: u"'", 13: u'^',
    16: u'q', 17: u'w', 18: u'e', 19: u'r', 20: u't', 21: u'z', 22: u'u', 23: u'i', 24: u'o', 25: u'p', 26: u'ü', 27: u'"',
    30: u'a', 31: u's', 32: u'd', 33: u'f', 34: u'g', 35: u'h', 36: u'j', 37: u'k', 38: u'l', 39: u'ö', 40: u'ä', 41: u'§',
    43: u'<', 44: u'y', 45: u'x', 46: u'c', 47: u'v', 48: u'b', 49: u'n', 50: u'm', 51: u',', 52: u'.', 53: u'-', 57: u' '
}

capscodes = {
    2: u'+', 3: u'"', 4: u'*', 5: u'ç', 6: u'%', 7: u'&', 8: u'/', 9: u'(', 10: u')', 11: u'=', 12: u'?', 13: u'`',
    16: u'Q', 17: u'W', 18: u'E', 19: u'R', 20: u'T', 21: u'Z', 22: u'U', 23: u'I', 24: u'O', 25: u'P', 27: u'!',
    30: u'A', 31: u'S', 32: u'D', 33: u'F', 34: u'G', 35: u'H', 36: u'J', 37: u'K', 38: u'L', 40: u'>', 41: u'°', 43: u'£',
    44: u'Y', 45: u'X', 46: u'C', 47: u'V', 48: u'B', 49: u'N', 50: u'M', 51: u';', 52: u':', 53: u'_', 57: u' '
}

CLIENTS = set()

conn = sqlite3.connect(cfg['DBPATH'])
conn.row_factory = dict_factory
db = conn.cursor()

asyncio.async(websockets.serve(ws_server, cfg['LISTEN'], 1234, ping_interval=None))
asyncio.async(get_qr_scanner_code())

loop = asyncio.get_event_loop()
loop.run_forever()
