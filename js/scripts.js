function update_stock_insert_form() {
    var data;
    var re = new RegExp('^' + $('#name').val() + '$',"gi");
    for(item of JSON.parse(sessionStorage.getItem('beers'))){
        if(item.beer.match(re))
        {
            data = item;
        }
    }

    if(data)
    {
        for(item of JSON.parse(sessionStorage.getItem('breweries'))){
            if(item.id == data.brewery)
            {
                $('#brewery').val(item.brewery);
                $('#country').val(item.country);
            }
        }
        for(item of JSON.parse(sessionStorage.getItem('styles'))){
            if(item.id == data.style)
            {
                $('#style').val(item.style);
            }
        }
        $('#abv').val(data.abv);
        $('#info').val(data.info);

        if ( data.tag_ba     == "on" ) { $('#tag_ba').prop('checked',true);     } else { $('#tag_ba').prop('checked',false);     }
        if ( data.tag_brut   == "on" ) { $('#tag_brut').prop('checked',true);   } else { $('#tag_brut').prop('checked',false);   }
        if ( data.tag_bret   == "on" ) { $('#tag_bret').prop('checked',true);   } else { $('#tag_bret').prop('checked',false);   }
        if ( data.tag_fruit  == "on" ) { $('#tag_fruit').prop('checked',true);  } else { $('#tag_fruit').prop('checked',false);  }
        if ( data.tag_pastry == "on" ) { $('#tag_pastry').prop('checked',true); } else { $('#tag_pastry').prop('checked',false); }

        $('#tag_ba').checkboxradio( "refresh" );
        $('#tag_brut').checkboxradio( "refresh" );
        $('#tag_bret').checkboxradio( "refresh" );
        $('#tag_fruit').checkboxradio( "refresh" );
        $('#tag_pastry').checkboxradio( "refresh" );

    }
}

function ws_get(what) {
    //console.log(what);
    if (arguments.length == 2) {
        websocket.send(JSON.stringify({'action': what, 'data': arguments[1]}));
        //console.log(JSON.stringify({'action': what, 'data': arguments[1]}));
    }
    else {
        websocket.send(JSON.stringify({'action': what}));
    }
}

function display(what,data,output) {
    while (true) {
        elem = $('script:last-of-type').next()
        if (elem.length == 1)
            {elem.remove()}
        else
            {break}
    }
    while (true) {
        elem = $('#dialog-form')
        if (elem.length == 1)
            {elem.remove()}
        else
            {break}
    }

    Handlebars.registerHelper('date', function (datestring) {
        var d = new Date(datestring);
        return d.getDate() + '.' + (d.getMonth()+1) + '.' + d.getFullYear();
    })
    Handlebars.registerHelper('datebest', function () {
        var d = new Date(new Date().getTime()+(1000*3600*24*7*8));
        return d.toISOString().split('T')[0];
    })

    var template = Handlebars.compile($("#template_"+what).html());
    $(output).html(template(data));
    if(what == "stock") {
        $('table.stock').on("click","tr",function(ev) {
            var beerid = $(ev.currentTarget).data("beerid");
            if(beerid) {
                ws_get('stock_info',beerid)
            }
        });
        dialog = $( "#dialog-form" ).dialog({
            autoOpen: false,
            height: 415,
            width: 1000,
            modal: true,
            resizable: false,
            draggable: false,
            buttons: {
                "Filter anwenden": function() {
                    dialog.dialog( "close" );
                    console.log("filter anwenden");
                },
                "Filter entfernen": function() {
                    dialog.dialog( "close" );
                    console.log("filter entfernen");
                }
            },
            close: function() {
                console.log("dialog schliessen");
            }
        });

        $( "#output > table th" ).on( "click", function() {
            dialog.dialog( "open" );
            ws_get('stock_dialog_filter');
            $("#dialog-form input[type=checkbox]").checkboxradio({icon: false});
            $("#filter-abv-slider").slider({
                range: true,
                min: 0,
                max: 30,
                values: [4, 20],
                create: function (event, ui) {
                    var value1 = $("#filter-abv-slider").slider("values", 0);
                    var value2 = $("#filter-abv-slider").slider("values", 1);
                    $("#filter-abv-slider").find(".ui-slider-handle:first").text(value1);
                    $("#filter-abv-slider").find(".ui-slider-handle:last").text(value2);
                },
                slide: function (event, ui) {
                    var value1 = $("#filter-abv-slider").slider("values", 0);
                    var value2 = $("#filter-abv-slider").slider("values", 1);
                    $("#filter-abv-slider").find(".ui-slider-handle:first").text(value1);
                    $("#filter-abv-slider").find(".ui-slider-handle:last").text(value2);
                }
            });
        });
    }
    if(what == "stock_change") {
        $('div.action-buttons').on("click","button",function(ev) {
            var action = $(ev.currentTarget).data("action");
            if(action == "stock_change_save") {
                ws_get('stock_change_save', [$('#change_storage').val(), $('#output h1:nth-child(2)').data('id')] );
            }
            if(action == "cancel") {
                ws_get('stock');
            }
        });
    }
    if(what == "stock_insert") {
        sessionStorage.setItem('styles', JSON.stringify(data.data['styles']) );
        sessionStorage.setItem('breweries', JSON.stringify(data.data['breweries']) );
        sessionStorage.setItem('beers', JSON.stringify(data.data['beers']) );
        $("input[type=checkbox]").checkboxradio({icon: false});
        $('div.action-buttons').on("click","button",function(ev) {
            var action = $(ev.currentTarget).data("action");
            if(action == "stock_insert") {
                //console.log($( "form" ).serializeArray())
                if ( $('#name').val() == ""
                    || $('#brewery').val() == ""
                    || $('#style').val() == ""
                    || $('#abv').val() == ""
                    || $('#amount').val() == "" ) {
                    alert("form not complete!");
                }
                else
                {
                    ws_get('stock_insert_save',$( "form" ).serializeArray());
                }
            }
            if(action == "cancel") {
                ws_get('stock');
            }
        });
        $("#name").autocomplete({
            source: function( request, response ) {
                data = []
                var re = new RegExp(request.term,"gi");
                for(a of JSON.parse(sessionStorage.getItem('beers'))){
                    if(a.beer.match(re))
                    {
                        data.push(a.beer)
                    }
                }
                response( data );
            },
            minLength: 0,
            select: function( event, ui ) {
                update_stock_insert_form();
            },
            change: function( event, ui ) {
                update_stock_insert_form();
            }
        });
        $("#brewery").autocomplete({
            source: function( request, response ) {
                data = []
                var re = new RegExp(request.term,"gi");
                for(a of JSON.parse(sessionStorage.getItem('breweries'))){
                    if(a.brewery.match(re))
                    {
                        data.push(a.brewery)
                    }
                }
                response( data );
            },
            minLength: 0
        });
        $("#style").autocomplete({
            source: function( request, response ) {
                data = []
                var re = new RegExp(request.term,"gi");
                for(a of JSON.parse(sessionStorage.getItem('styles'))){
                    if(a.style.match(re))
                    {
                        data.push(a.style)
                    }
                }
                response( data );
            },
            minLength: 0
        });
    }

    if(what == "qrcode") {
        $('div.action-buttons').on("click","button",function(ev) {
            var action = $(ev.currentTarget).data("action");
            if(action == "stock_restock") {
                ws_get('stock_restock',$('#output h1:nth-child(2)').data('id'))
            }
            if(action == "stock_checkout") {
                ws_get('stock_checkout',$('#output h1:nth-child(2)').data('id'))
            }
            if(action == "stock_change") {
                ws_get('stock_change',$('#output h1:nth-child(2)').data('id'))
            }
            if(action == "cancel") {
                ws_get('stock');
            }
        });
    }
}
function setup_dialog(what,data) {
    if(what == "stock_dialog_filter") {
        console.log("dialog_filter")
        sessionStorage.setItem('styles', JSON.stringify(data.data['styles']) );
        sessionStorage.setItem('breweries', JSON.stringify(data.data['breweries']) );
        sessionStorage.setItem('beers', JSON.stringify(data.data['beers']) );
        sessionStorage.setItem('countries', JSON.stringify(data.data['countries']) );
        $("#filter-beer").autocomplete({
            source: function( request, response ) {
                data = []
                var re = new RegExp(request.term,"gi");
                for(a of JSON.parse(sessionStorage.getItem('beers'))){
                    if(a.beer.match(re))
                    {
                        data.push(a.beer)
                    }
                }
                console.log(data)
                response( data );
            },
            minLength: 0,
            select: function( event, ui ) {
                console.log('item selected');
            },
            change: function( event, ui ) {
                console.log('item selected');
            }
        });
        $("#filter-brewery").autocomplete({
            source: function( request, response ) {
                data = []
                var re = new RegExp(request.term,"gi");
                for(a of JSON.parse(sessionStorage.getItem('breweries'))){
                    if(a.brewery.match(re))
                    {
                        data.push(a.brewery)
                    }
                }
                response( data );
            },
            minLength: 0
        });
        $("#filter-style").autocomplete({
            source: function( request, response ) {
                data = []
                var re = new RegExp(request.term,"gi");
                for(a of JSON.parse(sessionStorage.getItem('styles'))){
                    if(a.style.match(re))
                    {
                        data.push(a.style)
                    }
                }
                response( data );
            },
            minLength: 0
        });
        $("#filter-country").autocomplete({
            source: function( request, response ) {
                data = []
                var re = new RegExp(request.term,"gi");
                for(a of JSON.parse(sessionStorage.getItem('countries'))){
                    if(a.country.match(re))
                    {
                        data.push(a.country)
                    }
                }
                response( data );
            },
            minLength: 0
        });
    }    
}

function setupApp( jQuery ) {
    websocket = new WebSocket("ws://"+window.location.hostname+":1234/");
    websocket.onmessage = function (event) {
        data = JSON.parse(event.data);
        switch (data.type) {
            case 'stock_insert':
                display('stock_insert',data,'#output');
                break;
            case 'stock':
                display('stock',data,'#output');
                break;
            case 'stock_info':
                display('stock_info',data,'#output2');
                break;
            case 'stock_dialog_filter':
                setup_dialog('stock_dialog_filter',data);
                break;
            case 'stats':
                display('stats',data,'#output');
                break;
            case 'qrcode':
                display('qrcode',data,'#output');
                break;
            case 'stock_change':
                display('stock_change',data,'#output');
                break;
            default:
                console.log("unsupported event:");
                console.log(data);
        }
    }; // end of websocket.onmessage
    websocket.onopen = function (event) {ws_get('stock')};

    $('nav a[href="#stock_insert"]').click(function(){ws_get('stock_insert')})
    $('nav a[href="#stock"]').click(function(){ws_get('stock')})
    $('nav a[href="#stats"]').click(function(){ws_get('stats')})
}; // end of setupApp
 
$(document).ready(setupApp);
