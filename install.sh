#!/bin/bash

sudo mkdir -p /opt/beerventory

sudo mv websocket.py /opt/beerventory/ 2>/dev/null
sudo mv config.py /opt/beerventory/ 2>/dev/null
sudo mv db.sqlite /opt/beerventory/ 2>/dev/null
sudo mv SourceCodePro-Bold.ttf /opt/beerventory/ 2>/dev/null
sudo mv beerventory.service /etc/systemd/system/ 2>/dev/null

sudo chmod +x /opt/beerventory/websocket.py

sudo systemctl daemon-reload
sudo systemctl enable --now beerventory
sudo systemctl restart beerventory
